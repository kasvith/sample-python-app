# Returns hello world from method
def helloWorld():
    return 'hello world'

if __name__ == '__main__':
    # print it when running
    print(helloWorld())
