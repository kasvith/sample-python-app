import unittest
import hello

# Initialize a class to use with unittest framework
class HelloTest(unittest.TestCase):

    # Test the hello world function from hello.py
    def testHelloWorld(self):
        # check whether the 'hello world' is returned by the methof
        self.assertEqual(hello.helloWorld(), 'hello world')

# run unit test
if __name__ == '__main__':
    unittest.main()
